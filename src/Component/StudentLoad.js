import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Student from "../pages/Student"
import Loading from './Loading';
import { Button, Col, Container, Form, InputGroup, Row } from 'react-bootstrap';

const StudentLoad = () => {
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const fetchData = async () => {
      try {
        const response = await axios.get('http://localhost:8095/rest/demo/getStudent');

        setTimeout(() => {
          setData(response.data.student);
          setIsLoading(false);
        }, 1500);
      } catch (error) {
        console.error('Error fetching data:', error);
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);

  const handleInputChange = (event) => {
    setKeyword(event.target.value);
  }

  const searchData = () => {
    setIsLoading(true);
    const fetchData = async () => {
      try {
        const response = await axios.get(`http://localhost:8095/rest/demo/search?keyword=${keyword}`);

        setTimeout(() => {
          setData(response.data);
          setIsLoading(false);
        }, 1500);
      } catch (error) {
        console.error('Error fetching data:', error);
        setIsLoading(false);
      }
    };

    fetchData();
  }

  return (
    <Container>      
      <div className='p-3'>
        {isLoading ? <Loading /> : <Student data={data} />}
      </div>
    </Container>

  );
};

export default StudentLoad;

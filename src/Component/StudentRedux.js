import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Student from "../pages/Student";
import { setData } from "../state/slices/StudentSlice";
import Loading from "./Loading";
import { Container } from "react-bootstrap";

const StudentRedux = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.student.data);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const fetchData = async () => {
      try {
        const response = await axios.get('http://localhost:8095/rest/demo/getStudent');

        setTimeout(() => {
          dispatch(setData(response.data));
          setIsLoading(false);
        }, 1500);
      } catch (error) {
        console.error('Error fetching data:', error);
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);

  return (
    <Container>
      <div display="flex justify-content">
        <div className="fs-2 text-center">Student Records with Redux</div>
        <div className='p-3'>
          {isLoading ? <Loading /> : <Student data={data} />}
        </div>
      </div>
    </Container>


  );
};

export default StudentRedux;
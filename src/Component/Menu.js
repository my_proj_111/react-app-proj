import { Button } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';


const Menu = () => {

/*   const [active, setActive] = useState(true);

  const handleClick = (e) => {
    e.preventDefault();
    setActive(true);
  }; */

  return (
    //<nav>

    <Navbar bg="info" data-bs-theme="light">
      <Container>
        <Nav className="me-auto fs-5 hover">
          <Nav.Link href="/"><Button variant="outline-light">
            <strong>Home</strong>
          </Button></Nav.Link>
          <Nav.Link href="/aboutus"><Button variant="outline-light">
            <strong>About Us</strong>
          </Button></Nav.Link>
          <Nav.Link href="/student"><Button variant="outline-light">
            <strong>Student Records</strong>
          </Button></Nav.Link>
          <Nav.Link href="/studentRedux"><Button variant="outline-light">
            <strong>Student Records Redux</strong>
          </Button></Nav.Link>
          <Nav.Link href="/contact"><Button variant="outline-light">
            <strong>Contact Us</strong>
          </Button></Nav.Link>
          <Nav.Link className="ms-auto" href="/login"><Button variant="outline-light">
            <strong>Login</strong>
          </Button></Nav.Link>
          <Nav.Link href="/test"><Button variant="outline-light">
            <strong>Node Js Data</strong>
          </Button></Nav.Link>
          <Nav.Link href="/pagination"><Button variant="outline-light">
            <strong>Pagination Test</strong>
          </Button></Nav.Link>
        </Nav>
      </Container>
    </Navbar>


    /* <Stack direction="horizontal" gap={3}>
    <ul className="nav-links">
      <li className="p-2"><Link to="/">Home</Link></li>
      <li className="p-2"><Link to="/aboutus">About</Link></li>         
      <li className="p-2"><Link to="/student">Student Records</Link></li>
      <li className="p-2"><Link to="/studentRedux">Student Records Redux</Link></li>
      <li className="p-2"><Link to="/contact">Contact Us</Link></li>
      <li className="p-2 ms-auto"><Link to="/login">Login</Link></li>
    </ul>
    </Stack> */
  );

}
export default Menu
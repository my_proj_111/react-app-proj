import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Student from "../pages/Student"
import Loading from './Loading';
import { Col, Container, Pagination, Row } from 'react-bootstrap';

const PaginationTest = () => {
    const [state, setState] = useState({
        data: [],
        size: 5,
        activePage: 1,
        pageNumber: 0
    });
    var totalPage = [1,2,3];
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        apiCall(state.pageNumber);
        
    }, []);

    const apiCall = (pageNumber) => {
        const fetchData = async () => {
            try {
                const response = await axios.get(`http://localhost:8095/rest/demo/getStudent?page=${pageNumber}&&size=${state.size}`);

                setTimeout(() => {
                    setState((prev) => ({
                        ...prev,
                        data: response.data
                    }));

                    //setData(response.data);
                    setIsLoading(false);
                }, 1500);
            } catch (error) {
                console.error('Error fetching data:', error);
                setIsLoading(false);
            }
        };

        fetchData();

        //setTotalPage(setState.data?.totalPage)
    }

    const handlePageChange = (pageNum) => {
        setState((prev) => ({...prev,  activePage: pageNum, pageNumber: pageNum }));
        setIsLoading(true);
        apiCall(pageNum-1);

        /* for (let index = 1; index <= state.data?.totalPage; index++) {
            totalPage.push(index);
        } */
    }



    return (
        <Container>
            <Row>
                <Col>
                    <div className="fs-2 text-left">Pagination</div>
                </Col>

            </Row>
            <div className='p-3'>
                {isLoading ? <Loading /> : <Student data={state.data.student} />}
            </div>

            <Pagination className="px-4">
                {totalPage.map((item, index) => {
                    return (
                        <Pagination.Item
                            onClick={() => handlePageChange(index + 1)}
                            key={index + 1}
                            active={index + 1 === state.activePage}
                        >
                            {index + 1}
                        </Pagination.Item>
                    )
                })}

            </Pagination>
        </Container>

    );
};

export default PaginationTest;

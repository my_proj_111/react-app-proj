

import Header from "../Layout/Header";
import AppFooter from "../Layout/AppFooter";
import { Route, Routes } from "react-router-dom";
import Login from "../pages/Login.js";
import Test from "../pages/Test.js";
import Home from "../pages/Home.js";
import AboutUs from "../pages/AboutUs";
import Menu from "./Menu.js";
import ContactUs from "../pages/ContactUs";
import StudentLoad from "./StudentLoad";
import StudentRedux from "./StudentRedux";
import PaginationTest from "./PaginationTest.js";

function PageRoutes({ data }) {

    return (
        <div>
            <Header data={data}></Header>
            <Menu/>
            <Routes>
                <Route exact path="/" element={<Home />} />
                <Route exact path="/aboutus" element={<AboutUs />} />
                <Route exact path="/login" element={<Login />} />
                <Route exact path="/student" element={<StudentLoad />} />
                <Route exact path="/studentRedux" element={<StudentRedux />} />
                <Route exact path="/contact" element={<ContactUs />} />
                <Route exact path="/test" element={<Test />} />
                <Route exact path="/pagination" element={<PaginationTest />} />
            </Routes>
            <AppFooter/>
        </div>
    )
}

export default PageRoutes;
import './../App.css';
import logo from './../Component/Assets/espireInfolabs.png'

const Header = ({data}) => {

    return(
       <div className="bg-info bg-gradient App-header">  
       <img src={logo} className="App-logo" alt="logo" />      
        <p className='header-text text-black'>{data}</p>
    </div>
    );
};

export default Header
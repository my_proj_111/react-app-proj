import { createSlice } from "@reduxjs/toolkit";

const initialState ={
    data:[],
};

export const StudentSlice = createSlice({
    name: 'student',
    initialState,
    reducers: {
        setData: (state, action) =>{
            state.data = action.payload;
        },
    },
});

export const {setData} = StudentSlice.actions;
export default StudentSlice.reducer;
import { configureStore } from "@reduxjs/toolkit";
import studentReducer from "./slices/StudentSlice"

export const store = configureStore({
    reducer: {
        student: studentReducer,
    }
})
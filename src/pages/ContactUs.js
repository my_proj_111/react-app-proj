import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { Container } from '@material-ui/core';

const ContactUs = () => {
  return (
    <Container className="p-3 w-50">
      <div className="fs-2">Contact Us</div>
      <Form>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridEmail">

            <Form.Control type="email" placeholder="Enter email" />
          </Form.Group>

          <Form.Group as={Col} controlId="formGridName">

            <Form.Control type="name" placeholder="Full Name" />
          </Form.Group>
        </Row>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridPhone">

            <Form.Control placeholder="Contact Number" />
          </Form.Group>
          <Form.Group as={Col} controlId="formGridJob">

            <Form.Control placeholder="Job Title" />
          </Form.Group>
        </Row>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridPhone">

            <Form.Control as="textarea" rows={3} placeholder="How can we help you?" />
          </Form.Group>

        </Row>

        <Button variant="primary" type="submit">
          Submit Your Request
        </Button>
      </Form></Container>
  )
}

export default ContactUs
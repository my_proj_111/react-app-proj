import React from 'react';
import Table from 'react-bootstrap/Table';

const Student = ({ data }) => {

  return (

    <>
      <Table striped hover size="sm">
        <thead>
          <tr>
            <th>Admission No</th>
            <th>Batch Code</th>
            <th>Roll No</th>
            <th>Full Name</th>
            <th>Father Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Date Of Joining</th>
          </tr>
        </thead>
<tbody>
  
{data?.length > 0 ? 
        data?.map((item, index) => {
          
          return (
            
              <tr key={index}>
                <td>{item.admNo}</td>
                <td>{item.batchCode}</td>
                <td>{item.rollNo}</td>
                <td>{item.fullName}</td>
                <td>{item.fatherName}</td>
                <td>{item.email}</td>
                <td>{item.phone}</td>
                <td>{item.dateOfJoin}</td>
              </tr>
          )
         
        }) : <div className="fs-5">No Data found</div>
      }
        </tbody>
      </Table>

 </>
  );

};

export default Student;

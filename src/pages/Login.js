import { useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";


const Login = () => {

	const [data, setData] = useState('Login Form');
	const [validated, setValidated] = useState(false);
	const [show, setShow] = useState();
	const [formData, setFormData] = useState({
		email: '',
		password: '',
	});

	const formValidate = (e) => {
		setShow("");
		const { name, value } = e.target;
		setFormData({
			...formData,
			[name]: value,
		});
	};

	const formSubmit = (event) => {
		const form = event.currentTarget;
		if (form.checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();

		} else {
			event.preventDefault();

			if (formData.email === "admin@admin.com" && formData.password === "admin123") {
				setShow("success");
			} else {
				setShow("failure");
			}
			event.stopPropagation();
		}
		setValidated(true);
	};

	/* const validateFormData = (data) => {
		const errors = {};

		// Email validation
		if (!data.email.trim()) {
			errors.email = 'email is required';
		} else if (!/^[a-zA-Z0-9]+$/.test(data.email)) {
			errors.email = 'email should contain only letters and numbers';
		} else if (/[^a-zA-Z0-9]/.test(data.email)) {
			errors.email = 'email should not contain special characters';
		}

		// Password validation
		if (!data.password.trim()) {
			errors.password = 'Password is required';
		} else if (data.password.length < 6) {
			errors.password = 'Password must be at least 6 characters long';
		} else if (/[^a-zA-Z0-9]/.test(data.password)) {
			errors.password = 'Password should not contain special characters';
		}

		return errors;
	}; */

	return (
		/* <div className="App">
			<hi>{data}</hi>
			<form action=""> 
				<div> 
					<label htmlFor="username">Username: </label>
					<input type="text" name="username" id="username"/> 
				</div> 
				<div> 
					<label htmlFor="passw">Password: </label>
					<input type="text" name="passw" id="passw"/> 
				</div>                                         
				<button type="submit">Login</button>                                                                                                                                                                                                                                                                                                                f                                                                                                                                                                                                                                                                                                                                          
			</form>
		</div> */
		<Container className="mt-5 w-50">


			{show === 'success' ? <div className="fs-5 bg-success bg-gradient rounded p-2 text-white">Login successfully!!</div> : show === 'failure' ? <div className="fs-5 bg-danger bg-gradient rounded p-2 text-white">Login Failed, Email or Password is incorrect!!</div> : <div></div>}

			<div className="fs-2">{data}</div>
			<div className='p-3'>
				<Form noValidate validated={validated} onSubmit={formSubmit}>
					<Form.Group className="mb-3" controlId="formGroupEmail">
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" name="email" placeholder="Enter email" value={formData.email} onChange={formValidate} required isInvalid={
							validated &&
							!/^\S+@\S+\.\S+$/.test(formData.email)
						} />
						<Form.Control.Feedback type="invalid">
							Please enter a valid email address.
						</Form.Control.Feedback>
					</Form.Group>
					<Form.Group className="mb-3" controlId="formGroupPassword">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" name="password" placeholder="Password" value={formData.password} onChange={formValidate} minLength={6} required isInvalid={
							validated && formData.password.length < 6
						} />

						<Form.Control.Feedback type="invalid">
							Password must be at lease 6 characters long.
						</Form.Control.Feedback>

					</Form.Group>
					<Button variant="primary" type="submit">
						Sign In
					</Button>
				</Form>
			</div>
		</Container>
	)
}

export default Login
import { Accordion } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
const Home = () => {

    return (
        <Container>
            <div className="fs-2">About Home</div>
            <div className='p-3'>

                <p>With over 2 decades of experience behind us, and global operations spread across 12 locations
                    worldwide - our Agile Digital Transformation Services help brands to be resilient to market
                    disruptions and focus on business outcomes and returns.</p>

                <p>We believe that true Digital Transformation can only be achieved with Total Experience (TX),
                    and it is the sum of Multi-Experience (MX), User Experience (UX), Customer Experience (CX),
                    and Employee Experience (EX).</p>

                <p>To make this possible, we adopt a cross-enterprise approach, backed by robust operations systems -
                    leading to meaningful customer engagements, retentions and increase in new customer acquisitions
                    for businesses. Thereby, we are the preferred partner for our customers, and we aim to become a
                    TX leader with end-to-end services of MX, UX, CX and EX.</p>

                <p>Our focus is to bring a positive impact to brand’s proﬁtability and revenue
                    streams, with emphasis on delivering engaging digital experiences, especially
                    when the world traverses through a new normal.</p>
            </div>

            <Accordion defaultActiveKey="0" flush>
                <Accordion.Item eventKey="0">
                    <Accordion.Header><div className='fs-5'>Customer Communications Management</div></Accordion.Header>
                    <Accordion.Body>
                        <p>Respond to rapidly changing consumer behaviour and demands by rethinking your customer communications strategy.
                            At Espire, we help organizations build strong ROI driven customer engagements with end-to-end customer communications management solutions.</p>

                        <p>Enable a unified omni-channel customer experience on all devices and channels with on-demand, interactive or automated batch communications.
                            Reach out to your customers through their preferred channels – print, text/email, digital, mobile, chatbots or storefront -and manage brand objectives,
                            content security and regulatory compliance. Personalize every step of the customer journey with relevant content powered by AI and ML.</p>
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                    <Accordion.Header><div className='fs-5'>Digital Workplace</div></Accordion.Header>
                    <Accordion.Body>
                        <p>Content Collaboration Platforms (CCP) and Content Services Platforms are the key enablers of the digital workplace suite of technologies
                            that drive business continuity today.</p>

                        <p>Espire’s suite of cloud based content services and collaboration tools will empower your employees to successfully navigate
                            the complexities of remote work. With access to content and information from anywhere, anytime and from any device, we ensure resilience
                            to business disruption. Our AI/ML enabled and cloud based Content Services for virtual teams encourage innovation and collaboration to help
                            stakeholders stay focused on business growth.</p>
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
        </Container>
    )
}

export default Home
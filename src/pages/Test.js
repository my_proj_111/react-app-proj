import axios from "axios";
import { useEffect, useState } from "react";
import { Container, Table } from "react-bootstrap"

const Test = () => {

    const [data, setData] = useState([]);

    useEffect(() => {
        
        const fetchData = async () => {
          try {
            const response = await axios.get('http://localhost:5001/api');
            setData(response.data);
            
          } catch (error) {
            console.error('Error fetching data:', error);            
          }
        };
    
        fetchData();
      }, []);


return(
    <Container>
        <div className="fs-2 text-left">Data from local node server</div>
    <Table striped hover size="sm">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
          </tr>
        </thead>
<tbody>
        {data?.map((item, index) => {
          
          return (
            
              <tr key={index}>
                <td>{item.id}</td>
                <td>{item.name}</td>
              </tr>
          )
         
        })}
        </tbody>
      </Table>
      </Container>
);
}

export default Test
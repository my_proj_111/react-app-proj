import Container from 'react-bootstrap/Container';

const AboutUs = () => {
    return (
        <Container>
            <div>
                <div className="fs-2">About Us</div>
                <div className='p-3'>

                    
                    <p>With our powerful self-serve solutions Marketing and CX leaders take complete charge of
                        delivering personalized, contextual and profitable customer experiences, seamlessly across digital &
                        physical brand touchpoints and customer communication channels.</p>

                    <p>Our key offering is our game-changing Customer Engagement Hub (CEH) that is driving intelligent design,
                        implementation, analysis, support and optimization of actionable customer journey maps for global brands.</p>

                    <p>Espire’s TX is built by seamlessly integrating all key enablers of excellent CX. It includes the complete
                        spectrum of digital and customer experience transformation solutions spanning Digital Experience Platforms (DXP),
                        Multi-Channel Customer Communication Management (MCCCM), Digital Workplace, Enterprise Solutions, Digital Engineering,
                        Enterprise-Wide Integration, Analytics, Cloud Services, Automation, Security Services. Along with this, our
                        complementary Cloud and Integration services ensure that CEH is agile and well integrated with client’s existing IT environment.</p>

                    <p>Espire also offers a wide spectrum of standalone services for DXM, Customer Communications Management,
                        Digital, Experience design, Enterprise applications, Applications management, Analytics, Integration, IoT and Cloud services.</p>

                </div>
            </div>
        </Container>
    )
}

export default AboutUs
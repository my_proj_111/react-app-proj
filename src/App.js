import './App.css';
import { useState } from 'react';
import PageRoutes from './Component/PageRoutes';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Component/ComponentStyle.css';
import './Layout/LayoutStyle.css';
import './pages/PageStyle.css';


function App() {

  const [data, setData] = useState('First React Application');

  return (
    <div  className="bg-info bg-gradient bg-opacity-50">
      <PageRoutes data={data}></PageRoutes>
      
    </div>
    /*<div className="App">
    {
    <login></login>
     <header className="App-header">
       <img src={logo} className="App-logo" alt="logo" />
       <p>
         Edit <code>src/App.js</code> and save to reload.
       </p>
       <a
         className="App-link"
         href="https://reactjs.org"
         target="_blank"
         rel="noopener noreferrer"
       >
         Learn React
       </a>
     </header> }
   </div>*/
  );
}

export default App;
